# Quelques autres propositions de travaux

Ces travaux ne sont pas en ligne, ou bien l'adresse n'est pas fourni ici. 
Demandez, nous les avons sous la main.

## Débutants

- Exercices de prise en main du langage Python, sur la plate forme Inginious
- Exercices de programmation Python, sur la plate forme Inginious

## Moyen

- Notebook sur le traitement d'images (filtres à réaliser)
- Notebook sur la cryptographie historique
- [Série de challenges 1](https://ensip.gitlab.io/pages-info/ressources/chal/chal01.html)
- [Série de challenges 2](https://ensip.gitlab.io/pages-info/ressources/chal/chal02.html)
- [Début de l'advent of code 2015](https://ensip.gitlab.io/pages-info/ressources/chal/advent_of_code_2015.html)
- [Pydéfis](https://pydefis.callicode.fr)

## Plus avancé

- [Pydéfis](https://pydefis.callicode.fr)
- Problème de la mosaïque persanne

