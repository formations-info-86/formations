# Quelques supports sur le réseau et la crypto

## Documents issus de la formation DIU-EIL (Poitiers + AEFE)

- Synthèse (courte)
  - [Réseaux, cryptographie - Survol](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_XX_survol.pdf)
- Fiches de cours réseaux
  - [Gouvernance d’Internet](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_01_admin_internet.pdf)
  - [Modèles en couches Osi et Tcp/Ip](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_00_modele_couches.pdf)
  - [Couche Réseau (Internet)](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_02_couche_reseau.pdf)
  - [Couche Transport](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_03_couche_transport.pdf)
  - [Couche d’accès au réseau](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_04_acces_reseau.pdf)
  - [Couche Application](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_05_couche_application.pdf)
  - [Protocoles de routage RIP et OSPF](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_20_rip_ospf.pdf)
- Fiches de cours Crypto 
  - [Histoire de la cryptographie](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_06_crypto_histoire.pdf)
  - [Cryptographie symétrique](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_07_crypto_sym.pdf)
  - [Cryptographie asymétrique](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_08_crypto_asym.pdf)
  - [Condensés](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_09_crypto_condenses.pdf)
  - [Utilisation de la cryptographie](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_10_crypto_utilisation.pdf)
- Exercices (réseaux et crypto)
  - [Fiche Exercices 1 – Pile TCP/IP - Wireshark](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/E_4_01_TCPIP.pdf)
  - [Fiche Exercice 2 – Dissection d’une capture réseau](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/E_4_02_wireshark.pdf)
  - [Fiche Exercice 3 – RSA jouet](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/E_4_03_RSA_jouet.pdf)
  - [Exercices – Quelques challenges](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/E_4_04_challenges.pdf)
  - [Fiche Exercices 5 – RSA](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/E_4_05_RSA_pico.pdf)
  - [Fiche exercices 6 – Routage](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/E_4_06_routage.pdf)

## Autres

Un exemple de TP qui peut être fait avec des étudiants (sur TCP/IP) est visible ici :

- [Pile TCP/IP première partie](https://ensip.gitlab.io/pages-info/cours/3a/reseaux/tp_tcpip_part1_new.html)
- [Pile TCP/IP seconde partie](https://ensip.gitlab.io/pages-info/cours/3a/reseaux/tp_tcpip_part2_new.html)

