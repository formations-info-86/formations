# Formation NSI 2022-023

Rejoindre l'équipe Framateam `info-poitiers` : <https://framateam.org/signup_user_complete/?id=7t4m4ofbh3g7denug65eor4xoe>
(Lien valide seulement quelques jours)

## Banque d'exercices / Git

- [Fiche : GIT](https://formations-info-86.gitlab.io/nsi_2022/PDF/C_git.pdf)
- [Fiche : Banque d'exercices](https://formations-info-86.gitlab.io/nsi_2022/PDF/C_banque_exercices_git.pdf)
- [Activité : Travail à faire sur GIT](git.md)
- [Activité : Banque d'exercices](banque_exercices.md)

## Présentation de projets NSI

Par les participants

## Intelligence artificielle

- [Fiche : Introduction au machine learning](https://formations-info-86.gitlab.io/nsi_2022/PDF/C_ia_intro.pdf) 
- [Fiche : IA, résultats…](https://formations-info-86.gitlab.io/nsi_2022/PDF/C_ia_resultats.pdf)
- [Fiche : Arbres de décision](https://formations-info-86.gitlab.io/nsi_2022/PDF/C_decision_tree.pdf)
- Activité débranchée : lien [Wooclap](https://www.wooclap.com/TETEIA) 
- Activité : algorithmes de machine learning [Dépôt `machine_learning_01`](https://gitlab.com/formations-info-86/machine_learning_01/) : notebook k-plus-proches voisins, arbres de décision, algorithme K-means
- [Fiche : Expérience avec ChatGPT](https://formations-info-86.gitlab.io/nsi_2022/PDF/C_experience_chatgpt.pdf)
- [Liens, Références, vidéos, livre...](ia.md)

<!--
- [Fiche : Réseaux de neurones](https://formations-info-86.gitlab.io/nsi_2022/PDF/C_rn.pdf)
- [Fiche : Réseaux multicouches et rétropropagation du gradient](https://formations-info-86.gitlab.io/nsi_2022/PDF/C_retropropagation.pdf)
- Activité : Réseaux de neurones [Dépôt `neural_nets_01`](https://gitlab.com/formations-info-86/neural_nets_01/) : programmation d'un perceptron multi-couche, rétropropagation du gradient, utilisation de keras/scikit-learn, réseaux convolutifs
-->

Compléments : 

- [Tableau blanc](https://browserboard.com/whiteboard/bAzUUNwbYTK9YHGHQhNr5M?key=Zdo7cWwredktqkSbkAfPHV)
- documents d'origine (MMI Lyon) sur l'activité débranchée :  [Entrez dans la tête d'une IA](https://mmi-lyon.fr/?site_ressource_peda=entrez-dans-la-tete-dune-ia)

## Travaux supplémentaires

Des exercices, des défis, des problèmes ? Sur des images, des sons, des graphes, des nombres, du texte ?
Demandez...

## Futures formations ?

[Merci de remplir ce questionnaire (il y a 1 seule questions :) )](https://framaforms.org/prochaine-formations-1666184030)
