# Intelligence artificielle

## :new:

- [Une vidéo (Monsieur Phi) qui sort du lot](https://apprendre-en-ligne.net/bloginfo/index.php/2023/04/24/2383-gpt-4-est-il-incontrolable-mr-phi) au sujet de ChatGPT
- [Rapport State of AI sur l'année 2022](https://spectrum.ieee.org/state-of-ai-2023)
- [ThisXdoesnotexist](https://thisxdoesnotexist.com/)
- [GPT Zero](http://gptzero.me/) puis naviguer dans la page et trouver la version *Classic* (par d'enregistrement)  : permet de détecter si un texte a été généré par une IA (Note : mes premiers textes ne sont pas très concluants, il semblerait que ce que je rédige ressemble plus à un texte automatique que ce qui est rédigé par une IA... c'est vexant).

## Cité dans la présentation sur l'IA 

- [ChatGPT](https://chat.openai.com)
- [Gaugan2](https://www.youtube.com/watch?v=p9MAvRpT6Cg)
- [Deepdream generator](https://deepdreamgenerator.com/)
- [Generated photos](https://generated.photos/faces)
- [GFP-GAN](https://app.baseten.co/apps/QPp4nPE/operator_views/RqgOnqV)
- [Dall-E2](https://openai.com/dall-e-2/) 
- Make A Scene (Meta), Imagen (Google), Midjourney
- [Make-a-Video](https://makeavideo.studio/)
- [Tableau d'une IA vendu 400 000 $](https://www.sciencesetavenir.fr/high-tech/intelligence-artificielle/un-tableau-peint-par-une-ia-vendu-a-plus-de-400-000-dollars_128993)
- [Une IA de Huawei termine la symphonie inachevée de Schubert](https://consumer.huawei.com/au/campaign/unfinishedsymphony/) (avec l'aide d'un compositeur...)
- [This person does not exist](https://thispersondoesnotexist.com/)
- [Moralmachines](http://moralmachine.mit.edu/hl/fr)
- [Tests reconnaissance faciale à Nice en 2019](https://www.franceinter.fr/reconnaissance-faciale-officiellement-interdite-elle-se-met-peu-a-peu-en-place)
- [Discrimination ethnique](https://www.francetvinfo.fr/replay-radio/le-monde-est-a-nous/en-chine-les-ouighours-etouffes-par-la-reconnaissance-faciale_4197391.html)
- [Caméras expérimentales dans les écoles](https://www.francetvinfo.fr/replay-radio/en-direct-du-monde/en-chine-des-uniformes-connectes-pour-suivre-les-eleves-a-distance_4763725.html)
- [Reconnaissance faciale et papier toilette](https://www.francetvinfo.fr/choix/video-les-toilettes-de-pekin-se-mettent-a-la-reconnaissance-faciale-pour-eviter-la-surconsommation-de-papier-hygienique_2145918.html)
- [Prédire la criminalité](https://www.lebigdata.fr/ia-predire-crimes-experts)
- [Arrestation de Nijeer Parks(2019)](https://www.lebigdata.fr/arrestation-erreur-reconnaissance-faciale)
- [Règles d'accord de crédit fantaisistes (2019)](https://www.01net.com/actualites/l-apple-card-est-elle-sexiste-1804746.html)
- [Camion ou autruche ?](https://www.nature.com/articles/d41586-019-03013-5)
- [Lunettes et reconnaissance faciale](https://users.ece.cmu.edu/~lbauer/papers/2016/ccs2016-face-recognition.pdf)
- [L'origine du monde censurée](https://www.francetvinfo.fr/culture/arts-expos/peinture/l-origine-du-monde-de-courbet-censure-sur-facebook-fin-du-differend-avec-un-internaute_3560629.html), 
- [Photographie de presse (Viet-Nam) censurée](https://ifex.org/fr/la-censure-par-facebook-de-la-photo-emblematique-de-la-guerre-du-vietnam-est-une-atteinte-a-la-liberte-dinformer/), 
- [Massacre de Christchurch](https://www.franceinter.fr/emissions/l-edito-m/l-edito-m-25-mars-2019))
- [Les bons conseils d'Alexa](https://www.presse-citron.net/cap-ou-pas-cap-alexa-dit-a-cette-fillette-de-mettre-ses-doigts-dans-une-prise-220v/)
- [Prix artistique pour MidJourney](https://www.01net.com/actualites/un-tableau-cree-par-une-ia-remporte-le-premier-prix-dune-competition-dart-et-declenche-la-polemique.html)

## Bibliographie / Webographie

## Références post-formation 

- [L'IA de A à Z](https://atozofai.withgoogle.com/) (en anglais) : courtes définitions agréablement présentées
- [TraAms 2021-2022 académie de Poitiers sur l'IA](https://ww2.ac-poitiers.fr/math/spip.php?article1171)
- [Ressources de la CNIL](https://www.cnil.fr/fr/intelligence-artificielle-ia)
- [Numéro hors série de Pour la science sur l'IA](https://boutique.groupepourlascience.fr/hors-serie-pour-la-science/magazine?_ga=2.127238349.1104059458.1649877835-1561762842.1649877834), dont un article est [disponible en ligne](https://www.pourlascience.fr/sd/informatique/reseaux-de-neurones-theorie-en-construction-23648.php) et télégeargeable en PDF.
- [Pour la science : l'interview de Serge Abiteboul est librement consultable](https://www.pourlascience.fr/sd/informatique/j-ai-un-probleme-je-ne-sais-pas-trop-ce-qu-est-l-intelligence-artificielle-23682.php)

## Vidéos

- :new: [Comment ces IA inventent-elles des images ?](https://www.youtube.com/watch?v=tdelUss-5hY), vidéo de David Louapre
- :new: [Midjourney, Dall.E ou artiste humain ?](https://www.youtube.com/watch?v=C7BnrNqhE-Q), vidéo Numérama 
- :new: [De quoi ChatGPT est-il VRAIMENT capable ?](https://www.youtube.com/watch?v=R2fjRbc9Sa0), vidéo de Monsieur Phi, pas particulièrement technique, mais avec un recul très intéressant sur ce que fait ChatGPT.
- [Video AUDIMath : Intelligence artificielle par apprentissage automatique](https://www.youtube.com/watch?v=qO00I8vU81A), Francis Bach
- [Vidéos Deepmath](https://www.youtube.com/c/deepmath) et ouvrage associé (voir plus loin)
- [Playlist Intelligence artificielle](https://youtube.com/playlist?list=PLtzmb84AoqRTl0m1b82gVLcGU38miqdrC), Lê Nguyên Hoang, Science 4 All (plus de 50 vidéos...)
- [Le deep learning](https://www.youtube.com/watch?v=trWrEWfhTVg), David Louapre, Science Étonnante
- [Cours de Y. Le Cun au collège de France](https://www.college-de-france.fr/site/yann-lecun/_inaugural-lecture.htm)

## En ligne

- [Apprentissage profond](https://atcold.github.io/pytorch-Deep-Learning/fr/), avec Python, Pytorch, des vidéos, des notebooks...
- [Convolutional Neural Networks for Visual Recognition](https://cs231n.github.io/convolutional-networks/),
- [Deepmath](https://exo7math.github.io/deepmath-exo7/), Arnaud Bodin et François Recher, groupe Exo7
- [Deeplearning](https://www.deeplearningbook.org/), Ian Goodfellow, Yoshua Bengio et Aaron Courville
- [Inceptionism: Going Deeper into Neural Networks](https://ai.googleblog.com/2015/06/inceptionism-going-deeper-into-neural.html) : article de vulgarisation de Google présentant les images DeepDream

## Livres à la consultation (pendant la formation)

- *Quand la machine apprend: La révolution des neurones artificiels et de l'apprentissage profond*, Yann Le Cun, Odile Jacob, 9782738149312
- *L'intelligence Artificielle*, Jean-Gabriel Ganascia, Flammarion, 2080351419
- *Intelligence, machines et mathématiques*, Ignasi Belda, coll. Le monde est mathématique, RBA
- *Dernières nouvelles de l'intelligence artificielle*, Rodolphe Gelin, Flammarion
- *Turing à la plage: L'intelligence artificielle dans un transat*, Rachid Guerraoui et Lê Nguyên Hoang, Dunod, 9782100795550
- *Intelligences Artificielles. Miroirs de nos vies*, Fibretigre, Arnold Zephir et Héloïse Chochois, Delcourt
- *La Petite Bédéthèque Des Savoirs - Tome 1 - L'intelligence Artificielle. Fantasmes Et Réalités*, Jean-Noël Lafargue et Marion Montaigne, Le Lombard, 2803636387
- *Deepmath*, Arnaud Bodin et François Recher, groupe Exo7, 9798692672872 (ouvrage disponible aussi [en ligne](https://exo7math.github.io/deepmath-exo7/))

