# Formations

Formations informatique

- [NSI 2022-2023](nsi_2022-2023/)
- [NSI 2021-2022](nsi_2022/)
- [Réseaux / Crypto, documents de formation du DIU-EIL](reseaux_2022/)
- [Anciens supports de formation DIU-EIL (Algorithmique, Python, Réseau, BDD...)](diueil/)

Pour tout un tas de raison, le travail proposé peut ne pas être adapté. Voyez dans [Autres](autres/) ce qu'on peut vous proposer. 
