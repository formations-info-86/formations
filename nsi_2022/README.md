#
Formation NSI 2022

## Intelligence artificielle, fiches de cours

- [Introduction au machine learning](https://formations-info-86.gitlab.io/nsi_2022/PDF/C_ia_intro.pdf) [Archive](PDF/C_ia_intro.pdf)
- [IA, résultats…](https://formations-info-86.gitlab.io/nsi_2022/PDF/C_ia_resultats.pdf) [Archive](PDF/C_ia_resultats.pdf)
- [Réseaux de neurones](https://formations-info-86.gitlab.io/nsi_2022/PDF/C_rn.pdf) [Archive](PDF/C_rn.pdf)
- [Réseaux multicouches et rétropropagation du gradient](https://formations-info-86.gitlab.io/nsi_2022/PDF/C_retropropagation.pdf) [Archive](PDF/C_retropropagation.pdf)
- [Arbres de décision](https://formations-info-86.gitlab.io/nsi_2022/PDF/C_decision_tree.pdf) [Archive](PDF/C_decision_tree.pdf)

### Intelligence artificielle, travail pratique

- Activité débranchée : [allsets.zip](activite_debranchee/allsets.zip) à télécharger et à dézipper. :new: Les documents d'origine sont disponible ici : [Entrez dans la tête d'une IA](https://mmi-lyon.fr/?site_ressource_peda=entrez-dans-la-tete-dune-ia) (désolé... j'ai dit IREM Grenoble... mais c'est la MMI :) )
- Lien [Wooclap](https://www.wooclap.com/TETEIA) pour l'activité
- Réseaux de neurones [Dépôt `neural_nets_01` branche 2022](https://gitlab.com/formations-info-86/neural_nets_01/-/tree/formation_2022) : programmation d'un perceptron multi-couche, rétropropagation du gradient, utilisation de keras/scikit-learn, réseaux convolutifs
- Autres algorithmes de machine learning [Dépôt `machine_learning_01` branche 2022](https://gitlab.com/formations-info-86/machine_learning_01/-/tree/formation_2022) : notebook k plus proches voisins, arbres de décision, algorithme K-means

[**tableau blanc**](https://browserboard.com/whiteboard/SEk99m5UTsCfyW6hsTHAUy?key=WG7bPhhVFzw8JZAaLokW8F)

## Banque d'exercices / Git

[Banque d'exercices](banque_exercices.md)


## Bibliographie / Webographie

### :new: Références post-formation :smile:

- [L'IA de A à Z](https://atozofai.withgoogle.com/) (en anglais) : courtes définitions agréablement présentées
- [TraAms 2021-2022 académie de Poitiers sur l'IA](https://ww2.ac-poitiers.fr/math/spip.php?article1171)
- [Ressources de la CNIL](https://www.cnil.fr/fr/intelligence-artificielle-ia)
- [Numéro hors série de Pour la science sur l'IA](https://boutique.groupepourlascience.fr/hors-serie-pour-la-science/magazine?_ga=2.127238349.1104059458.1649877835-1561762842.1649877834), dont un article est [disponible en ligne](https://www.pourlascience.fr/sd/informatique/reseaux-de-neurones-theorie-en-construction-23648.php) et télégeargeable en PDF.
- [Pour la science : l'interview de Serge Abiteboul est librement consultable](https://www.pourlascience.fr/sd/informatique/j-ai-un-probleme-je-ne-sais-pas-trop-ce-qu-est-l-intelligence-artificielle-23682.php)

### Vidéos

- [Video AUDIMath : Intelligence artificielle par apprentissage automatique](https://www.youtube.com/watch?v=qO00I8vU81A), Francis Bach
- [Vidéos Deepmath](https://www.youtube.com/c/deepmath) et ouvrage associé (voir plus loin)
- [Playlist Intelligence artificielle](https://youtube.com/playlist?list=PLtzmb84AoqRTl0m1b82gVLcGU38miqdrC), Lê Nguyên Hoang, Science 4 All (plus de 50 vidéos...)
- [Le deep learning](https://www.youtube.com/watch?v=trWrEWfhTVg), David Louapre, Science Étonnante
- [Cours de Y. Le Cun au collège de France](https://www.college-de-france.fr/site/yann-lecun/_inaugural-lecture.htm)

### En ligne

- [Apprentissage profond](https://atcold.github.io/pytorch-Deep-Learning/fr/), avec Python, Pytorch, des vidéos, des notebooks...
- [Convolutional Neural Networks for Visual Recognition](https://cs231n.github.io/convolutional-networks/),
- [Deepmath](https://exo7math.github.io/deepmath-exo7/), Arnaud Bodin et François Recher, groupe Exo7
- [Deeplearning](https://www.deeplearningbook.org/), Ian Goodfellow, Yoshua Bengio et Aaron Courville
- [Inceptionism: Going Deeper into Neural Networks](https://ai.googleblog.com/2015/06/inceptionism-going-deeper-into-neural.html) : article de vulgarisation de Google présentant les images DeepDream

### Livres à la consultation (pendant la formation)

- *Quand la machine apprend: La révolution des neurones artificiels et de l'apprentissage profond*, Yann Le Cun, Odile Jacob, 9782738149312
- *L'intelligence artificielle en 5 minutes par jour*, Stéphane D' Ascoli, First, 9782412059845
- *Turing à la plage: L'intelligence artificielle dans un transat*, Rachid Guerraoui et Lê Nguyên Hoang, Dunod, 9782100795550
- *Intelligences Artificielles. Miroirs de nos vies*, Fibretigre, Arnold Zephir et Héloïse Chochois, Delcourt
- *La Petite Bédéthèque Des Savoirs - Tome 1 - L'intelligence Artificielle. Fantasmes Et Réalités*, Jean-Noël Lafargue et Marion Montaigne, Le Lombard, 2803636387
- *L'intelligence Artificielle*, Jean-Gabriel Ganascia, Flammarion, 2080351419
- *Intelligence, machines et mathématiques*, Ignasi Belda, coll. Le monde est mathématique, RBA
- *Deepmath*, Arnaud Bodin et François Recher, groupe Exo7, 9798692672872 (ouvrage disponible aussi [en ligne](https://exo7math.github.io/deepmath-exo7/))

## Retour sur l'enquête

Score élevé => item beaucoup demandé.

```
Partie : Thèmes NSI
===============================
  20 Programmation Python (un peu plus) avancée
  18 Réseaux informatiques
  17 Unix
  13 IHM et Web
  12 Graphes
  11 Récursivité (+ prog dynamique etc.)
  10 Arbres
  06 Les algorithmes du programme NSI(tri etc..)
  03 Bases de données
  00 Structures de données Python (listes, tuples, dictionnaires...)
  -2 Codage de l'information

Partie : Thèmes connexes
===============================
  21 Intelligence artificielle
  07 Théorie des langages
  07 Formats de fichiers
  02 Retour sur les épreuves Capes NSI
  00 Expressions rationnelles

Partie : Enseignement de l'informatique
===============================
  22 Gestion du projet informatique
  18 Banque d'exercices
  16 Retour d'expérience des enseignants présents à la formation
  16 Comment évaluer les travaux des élèves ?
```
